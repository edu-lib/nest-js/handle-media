import {
  LIBRARY_SERVICE,
  TOKEN_PROVIDER_CONFIG,
} from '../resources/constants/const';
import { Provider } from '@nestjs/common';
import { ProvidersMedia } from '../resources/enums/providers.enum';
import { v2 as cloudinary } from 'cloudinary';

export const libraryFactory: Provider = {
  provide: LIBRARY_SERVICE,
  useFactory: (providerConfig: ProvidersMedia) => {
    switch (providerConfig) {
      case ProvidersMedia.CLOUDINARY:
        return cloudinary;
      default:
        throw new Error();
    }
  },
  inject: [TOKEN_PROVIDER_CONFIG],
};
