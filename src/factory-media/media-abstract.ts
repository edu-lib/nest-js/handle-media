import { Files } from '../resources/types/file';
import { UploadApiResponse } from "cloudinary";

export abstract class AbstractMedia {

  uploadFiles(image: Files | Files[]): Promise<UploadApiResponse> {
    return;
  }

  public fileToBase64(buffer: Buffer): string {
    return buffer.toString('base64');
  }
}
