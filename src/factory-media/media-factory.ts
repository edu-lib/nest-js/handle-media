import { Provider } from '@nestjs/common';
import { CloudinaryService } from '../cloudinary/cloudinary.service';
import { AbstractMedia } from './media-abstract';
import { ProvidersMedia } from '../resources/enums/providers.enum';
import {
  LIBRARY_SERVICE,
  TOKEN_PROVIDER_CONFIG,
} from '../resources/constants/const';

export const MediaFactory: Provider = {
  provide: AbstractMedia,
  useFactory: (optionsProvider: ProvidersMedia, mediaService) => {
    if (optionsProvider === ProvidersMedia.CLOUDINARY) {
      return new CloudinaryService(mediaService);
    } else {
      throw new Error('In order to use this module please add a provider');
    }
  },
  inject: [TOKEN_PROVIDER_CONFIG, LIBRARY_SERVICE],
};
