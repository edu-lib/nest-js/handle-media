import { AbstractMedia } from '../factory-media/media-abstract';
import { Error } from 'tslint/lib/error';
import { Files } from '../resources/types/file';
import { UploadApiResponse } from 'cloudinary';


export class CloudinaryService extends AbstractMedia {
  constructor(private cloudinary) {
    super();
    this.config();
  }

  private config(): void {
    try {
      this.cloudinary.config({
        cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
        api_key: process.env.CLOUDINARY_API_KEY,
        api_secret: process.env.CLOUDINARY_API_SECRET,
      });
      console.log('=========> Cloudinary config  is SUCCESS <============');
    } catch (e) {
      throw new Error('=========> Cloudinary config is ERROR <============');
    }
  }

  async uploadFiles(files: Files | Files[]): Promise<UploadApiResponse> {
    if (!Array.isArray(files)) files = [files];
    for (const file of files) {
      const mimeTypeB64 = `data:${file.mimetype};base64,`;
      const imageB64ToSave = `${mimeTypeB64}${this.fileToBase64(file.buffer)}`;
      console.log('uploading file to Cloudinary...');
      return await this.cloudinary.uploader.upload(
       imageB64ToSave,
       {
         folder: 'test',
         height: 250,
         width: 250,
         gravity: 'faces',
         crop: 'fill',
       },
     )
    }
  }

}
