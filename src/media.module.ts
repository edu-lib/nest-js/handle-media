import { DynamicModule, Module } from '@nestjs/common';
import { MediaFactory } from './factory-media/media-factory';
import { AbstractMedia } from './factory-media/media-abstract';
import { TOKEN_PROVIDER_CONFIG } from './resources/constants/const';
import { ProvidersMedia } from './resources/enums/providers.enum';
import { libraryFactory } from './factory-media/library-factory';

@Module({
  providers: [],
})
export class MediaModule {
  public static forRoot(provider: ProvidersMedia): DynamicModule {
    return {
      module: MediaModule,
      providers: [
        { provide: TOKEN_PROVIDER_CONFIG, useValue: provider },
        libraryFactory,
        MediaFactory,
      ],
      exports: [AbstractMedia],
    };
  }

}
