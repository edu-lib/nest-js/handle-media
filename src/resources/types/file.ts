export type Files = {
  fieldname: string;
  originalname: string;
  encoding: '7bit';
  mimetype: string | 'image/jpeg';
  buffer: Buffer;
  size: number;
};
